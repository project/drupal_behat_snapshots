@snapshot @api @drupal
Feature: snapshot
  In order to test the static page.
  As a site visitor.
  I need to open the page by URL and compare it with the preview result.

  Background:
    Given I am an anonymous user

  Scenario: check website pages

    Then I test listed pages:
      | Page name       | Page path      | Wait for the page to load | Wait for an element to be visible | Wait for text                                        | Hide elements            | Hide all images | Window breakpoints  |
      | Main page       | /              | 1                         | input[type=name]                  | Congratulations and welcome to the Drupal community. | #block-olivero-syndicate | true            | 300, 600, 1200      |
      | User login page | /user/login    | 2                         |                                   |                                                      |                          | true            | 300, 600, 1200      |
      | Reset password  | /user/password | 1                         |                                   |                                                      |                          | false           | 300, 600, 800, 1200 |
    And show pages testing report
