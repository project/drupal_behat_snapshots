<?php

declare(strict_types=1);

namespace Drupal\BehatSnapshots\Context;

/**
 * Trait SnapshotContextBrowser.
 *
 * This trait stores methods that could be use to interact with browser.
 */
trait WebPageInteractionTrait {

  /**
   * Function browserHideAllImages.
   *
   * Append the style element to the head of the document to hide all the
   * images on the page using CSS.
   *
   * @return void
   *   Returns nothing.
   */
  private function browserHideAllImages(): void {
    $disableImagesJs = <<<EOL
    let styleElement = document.createElement('style'),
    cssContent = `
        img {
            display: none !important;
        }
        *[style*="background-image"] {
            background-image: none !important;
        }
    `;
    styleElement.type = 'text/css';
    if (styleElement.styleSheet) {
        styleElement.styleSheet.cssText = cssContent; // For IE
    } else {
        styleElement.appendChild(document.createTextNode(cssContent)); // For other browsers
    }
    document.head.appendChild(styleElement);
EOL;
    // Disable page images.
    $this->getSession()->executeScript($disableImagesJs);
  }

  /**
   * Function browserHideElements.
   *
   * Hide the list of specified elements on the page.
   *
   * @param array $cssSelectors
   *   List of CSS selectors.
   *
   * @return void
   *   Returns nothing.
   */
  private function browserHideElements(array $cssSelectors): void {
    foreach ($cssSelectors as $selector) {
      // Check if the element exists.
      $elementExistsScript = sprintf('return document.querySelector("%s") !== null;', $selector);
      $elementExists = $this->getSession()->evaluateScript($elementExistsScript);

      // If the element exists, hide it.
      if ($elementExists) {
        $javascript = sprintf('document.querySelector("%s").style.display = "none";', $selector);
        $this->getSession()->executeScript($javascript);
      }
    }
  }

  /**
   * Function browserDisablePageFocus.
   *
   * Remove focus from any focused element on the page or blur
   * the currently focused element using native JavaScript.
   * Blinking cursor pointer could change the image screenshot.
   *
   * @return void
   *   Returns nothing.
   */
  private function browserDisablePageFocus(): void {
    $this->getSession()->executeScript('document.activeElement.blur();');
  }

  /**
   * Function browserWait.
   *
   * @param int $mSeconds
   *   Milliseconds.
   *
   * @return void
   *   Returns nothing.
   */
  private function browserWait(int $mSeconds = 500): void {
    $this->getSession()->wait($mSeconds);
  }

  /**
   * Function browserWaitForReadyState.
   *
   * @param int $timeout
   *   Milliseconds.
   *
   * @return void
   *   Returns nothing.
   */
  private function browserWaitForReadyState(int $timeout = 1000): void {
    $this->getSession()->wait($timeout, 'document.readyState === "complete"');
  }

  /**
   * Function browserScrollTo.
   *
   * Scroll browser window in 2 directions.
   *
   * @param int $x
   *   X coordinate.
   * @param int $y
   *   Y coordinate.
   *
   * @return void
   *   Returns nothing.
   */
  private function browserScrollTo(int $x = 0, int $y = 0): void {
    $this->getSession()
      ->executeScript('window.scrollTo(' . $x . ', ' . $y . ')');
    // Wait for scroll to complete using a listener for the 'scroll' event.
    $this->getSession()
      ->wait(500, 'typeof window.scrollComplete !== "undefined" && window.scrollComplete');
    $this->getSession()->wait(500);
  }

  /**
   * Function browserWaitForElementToBeVisible.
   *
   * @param array $cssSelectors
   *   CSS Selectors.
   * @param int $timeout
   *   Timeout.
   *
   * @return void
   *   Returns nothing.
   */
  private function browserWaitForElementToBeVisible(array $cssSelectors, int $timeout = 1000): void {
    foreach ($cssSelectors as $selector) {
      $this->getSession()->wait($timeout, sprintf(
        'document.querySelector("%s") !== null && ' .
        'document.querySelector("%s").offsetWidth > 0 && ' .
        'document.querySelector("%s").offsetHeight > 0',
        $selector,
        $selector,
        $selector
      ));
    }
  }

  /**
   * Function browserWaitForTextVisible.
   *
   * @param string $text
   *   Text.
   * @param int $timeout
   *   Timeout, ms.
   * @param int $interval
   *   Interval, ms, i.e. 500,000 microseconds = 0.5 seconds.
   *
   * @return void
   *   Returns nothing.
   */
  private function browserWaitForTextVisible(string $text, int $timeout = 10, int $interval = 500000): void {

    $startTime = microtime(TRUE);

    while (microtime(TRUE) - $startTime < $timeout) {
      $page = $this->getSession()->getPage();

      if ($page->hasContent($text)) {
        return;
      }

      usleep($interval);
    }

    throw new \RuntimeException("Text '{$text}' did not become visible within the timeout.");

  }

}
