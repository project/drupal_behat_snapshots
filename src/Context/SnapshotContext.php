<?php

declare(strict_types=1);

namespace Drupal\BehatSnapshots\Context;

use Behat\Gherkin\Node\TableNode;
use Drupal\BehatSnapshots\Contract\ImageComparatorInterface;
use Drupal\BehatSnapshots\Contract\SnapshotContextInterface;
use Drupal\BehatSnapshots\Service\ImageComparator;
use Drupal\BehatSnapshots\Service\ResultRenderer;
use Drupal\DrupalExtension\Context\MinkContext;

/**
 * Class SnapshotContext.
 *
 * This context provides Behat testing steps implementation
 * for the frontend testing with snapshot testing approach.
 * */
class SnapshotContext extends MinkContext implements SnapshotContextInterface {

  use WebPageInteractionTrait;

  /**
   * List of pages to test.
   *
   * @property array $pages
   */
  protected array $pages = [];

  /**
   * List of pages to test.
   *
   * @property array $pages
   */
  protected array $commandsToFix = [];

  /**
   * Path to the Behat files directory.
   *
   * @property string $filePath
   */
  private string $filePath;

  /**
   * Service that is used to compare images.
   *
   * @property ImageComparatorInterface $imageComparator
   */
  private ImageComparatorInterface $imageComparator;

  /**
   * Constructor.
   */
  public function __construct() {
    $this->imageComparator = new ImageComparator();
  }

  /**
   * Function iTestListedPages.
   *
   * @param \Behat\Gherkin\Node\TableNode $pages
   *   List of pages.
   *
   * @return void
   *   Returns nothing.
   *
   * @Then I test listed pages:
   *
   * @throws \Exception
   */
  public function iTestListedPages(TableNode $pages): void {
    $this->filePath = $this->getMinkParameter('files_path');

    if (!$this->filePath || !is_dir($this->filePath)) {
      throw new \Exception(sprintf('Directory error: %s', $this->filePath));
    }

    if (empty($pages->getHash())) {
      throw new \Exception('There are no pages provided.');
    }

    $this->setPages($pages);

    foreach ($this->pages as &$page) {
      // Started counting page testing time.
      $pageTestTimeStart = microtime(TRUE);

      $this->visitPath($page['path']);

      // This section is used to wait for some events to be processed and
      // hide some elements on the page if needed.
      $this->browserWaitForReadyState();

      // Optional. Force wait if it was specified.
      if ($page['condition']['wait']) {
        $this->browserWait($page['condition']['wait']);
      }

      if ($page['condition']['wait_for_elements']) {
        $this->browserWaitForElementToBeVisible($page['condition']['wait_for_elements']);
      }

      if ($page['condition']['wait_for_text']) {
        $this->browserWaitForTextVisible($page['condition']['wait_for_text']);
      }

      // Hide all the page images if they could be dynamic, it could
      // help us to evaluate the pages as equal even if the images are not.
      if ($page['condition']['hide_all_images']) {
        $this->browserHideAllImages();
      }

      // Optional.
      if ($page['condition']['hide_elements']) {
        $this->browserHideElements($page['condition']['hide_elements']);
      }

      // Disable focus on any page element.
      // Blinking cursor could case the image diff.
      $this->browserDisablePageFocus();

      $breakpoints = $page['window_breakpoints'];

      foreach ($breakpoints as $breakpointWidth) {
        // Resize the window using the breakpoint width specified in the test.
        $windowWidth = (int) $breakpointWidth;
        $windowHeight = 600;
        $this->getSession()->getDriver()->resizeWindow($windowWidth, $windowHeight);

        // Get the initial window width and height.
        $scrollWidth = $this->getSession()->evaluateScript('return document.documentElement.scrollWidth;');
        $scrollHeight = $this->getSession()->evaluateScript('return document.documentElement.scrollHeight;');

        // Sanitize the username to ensure it is a valid folder name.
        $rawPageName = filter_var($page['name'], FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $dirName = str_replace(' ', '-', strtolower($rawPageName));

        if (empty($dirName) || !preg_match('/^[a-zA-Z0-9_-]+$/', $dirName)) {
          throw new \Exception('This page title can not be used, please specify another one.');
        }

        $page['folder'] = $dirName;

        // Generate the proper path to the `expected` and `actual` folders.
        $pathToExpectedScreenshots = $this->filePath . '/' . $dirName . '/expected/' . $windowWidth . '/';
        $pathToActualScreenshots = $this->filePath . '/' . $dirName . '/actual/' . $windowWidth . '/';

        $isPathConfigured = TRUE;

        if (!is_dir($pathToActualScreenshots)) {
          $this->commandsToFix[] = 'mkdir -p ' . $pathToActualScreenshots;
          $isPathConfigured = FALSE;
        }

        if (!is_dir($pathToExpectedScreenshots)) {
          $this->commandsToFix[] = 'mkdir -p ' . $pathToExpectedScreenshots;
          $isPathConfigured = FALSE;
        }

        if (!$isPathConfigured) {
          continue;
        }

        // Clear ../actual/* folder files before new generation.
        $this->removeFilesInDir($pathToActualScreenshots);

        for ($i_width = 0; $i_width < $scrollWidth; $i_width += $windowWidth) {
          for ($i_height = 0; $i_height < $scrollHeight; $i_height += $windowHeight) {
            // Generate the file name based on the specified folder name.
            $fileName = $i_width . 'x' . $i_height . '.png';
            $fileNameDiff = $i_width . 'x' . $i_height . '.diff.png';

            $this->browserScrollTo($i_width, $i_height);

            $this->saveScreenshot($fileName, $pathToActualScreenshots);

            $actualImagePath = $pathToActualScreenshots . $fileName;
            $diffImagePath = $pathToActualScreenshots . $fileNameDiff;
            $expectedImagePath = $pathToExpectedScreenshots . $fileName;

            if (file_exists($expectedImagePath)) {
              if (!$this->imageComparator->areImagesEqual($actualImagePath, $expectedImagePath)) {
                $this->commandsToFix[] = 'cp -f ' . $actualImagePath . ' ' . $expectedImagePath;
                $this->commandsToFix[] = '# diff image: ' . $diffImagePath;
              }
            }
            else {
              $this->commandsToFix[] = 'cp -f ' . $actualImagePath . ' ' . $expectedImagePath;
            }
          }
        }
      }

      // Calculate the page testing time result.
      $pageTestTimeEnd = microtime(TRUE);
      // Dividing by 60*60 will give the execution time in seconds.
      $testExecutionTime = ($pageTestTimeEnd - $pageTestTimeStart) / 60 * 60;

      $page['test_time'] = $testExecutionTime;
    }

    if (!empty($this->commandsToFix)) {
      throw new \Exception(sprintf('Please execute these commands to update the expected results:' . PHP_EOL . '%s' . PHP_EOL, implode(PHP_EOL, $this->commandsToFix)));
    }
  }

  /**
   * Function showPagesTestingReport.
   *
   * @return void
   *   Returns nothing.
   *
   * @Then show pages testing report
   *
   * @throws \Exception
   */
  public function showPagesTestingReport(): void {
    $resultRenderer = new ResultRenderer($this->filePath);
    $resultRenderer->render($this->pages);
  }

  /**
   * Function setPages.
   *
   * @param \Behat\Gherkin\Node\TableNode $pages
   *   List of pages.
   *
   * @return void
   *   Returns nothing.
   */
  private function setPages(TableNode $pages): void {
    // Reset the pages list. Especially important if we use iTestListedPages
    // implementation several times in the .feature file.
    $this->pages = [];

    foreach ($pages->getHash() as $row) {
      $this->pages[] = [
        'name' => $row['Page name'],
        'path' => $row['Page path'],
        'window_breakpoints' => !empty($row['Window breakpoints']) ? explode(',', $row['Window breakpoints']) : [],
        'condition' => [
          'wait' => $row['Wait for the page to load'] ? 1000 * $row['Wait for the page to load'] : NULL,
          'wait_for_elements' => !empty($row['Wait for an element to be visible']) ? explode(',', $row['Wait for an element to be visible']) : NULL,
          'wait_for_text' => $row['Wait for text'] ?? NULL,
          'hide_all_images' => $row['Hide all images'] == 'true',
          'hide_elements' => !empty($row['Hide elements']) ? explode(',', $row['Hide elements']) : NULL,
        ],
      ];
    }
  }

  /**
   * Function removeFilesInDir.
   *
   * @param string $dir
   *   Directory path.
   *
   * @return void
   *   Returns nothing.
   */
  private function removeFilesInDir(string $dir): void {
    $iterator = new \DirectoryIterator($dir);
    // Loop through the files and remove each one.
    foreach ($iterator as $fileInfo) {
      if ($fileInfo->isFile()) {
        unlink($fileInfo->getPathname());
      }
    }
  }

}
