<?php

declare(strict_types=1);

namespace Drupal\BehatSnapshots\Service;

use Behat\Gherkin\Node\TableNode;
use Drupal\BehatSnapshots\Contract\ResultRendererInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ResultRenderer.
 *
 * This class is used to render results message.
 */
class ResultRenderer implements ResultRendererInterface {

  /**
   * Path to the Behat files directory.
   *
   * @property string $filePath
   */
  private string $filePath;

  /**
   * Constructor.
   */
  public function __construct(string $filePath) {
    $this->filePath = $filePath;
  }

  /**
   * Function render.
   *
   * @param array $pages
   *   Array of the pages.
   *
   * @return void
   *   Returns nothing.
   */
  public function render(array $pages):void {
    $tableRows = [
      [
        "\033[32m" . 'Page Name' . "\033[0m",
        'Path',
        'Screenshots Folder',
        "\033[32m" . 'Test Time, seconds' . "\033[0m",
      ],
    ];

    foreach ($pages as $page) {
      // Apply green color to the page name and test time.
      $highlightedPageName = "\033[32m{$page['name']}\033[0m";
      $highlightedTestTime = sprintf("\033[32m%.2f\033[0m", $page['test_time']);

      // Add data to the rows array.
      $tableRows[] = [
        $highlightedPageName,
        $page['path'],
        $this->filePath . '/' . $page['folder'] . '/actual/',
        $highlightedTestTime,
      ];
    }

    // Create a TableNode with the rows array.
    $tableNode = new TableNode($tableRows);
    $response = new Response($tableNode->getTableAsString());
    $response->send();
  }

}
