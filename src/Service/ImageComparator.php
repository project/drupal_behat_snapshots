<?php

declare(strict_types=1);

namespace Drupal\BehatSnapshots\Service;

use Drupal\BehatSnapshots\Contract\ImageComparatorInterface;

/**
 * Class ImageComparator.
 *
 * This class implements the image comparison logic.
 */
class ImageComparator implements ImageComparatorInterface {

  /**
   * Function imagesAreEqual.
   *
   * While PHP does not have native functions for advanced image comparison
   * that specifically handle dynamic or metadata changes, it is still possible
   * to use some basic image manipulation functions
   * to create a simplified comparison.
   *
   * @param string $actualImagePath
   *   Path to the first file.
   * @param string $expectedImagePath
   *   Path to the second file.
   *
   * @return bool
   *   Boolean result of comparison.
   *
   * @throws \Exception
   */
  public function areImagesEqual(string $actualImagePath, string $expectedImagePath): bool {
    if (!file_exists($actualImagePath)) {
      throw new \Exception(sprintf('This path does not exists: %s', $actualImagePath));
    }
    if (!file_exists($expectedImagePath)) {
      throw new \Exception(sprintf('This path does not exists: %s', $expectedImagePath));
    }

    if (!extension_loaded('gd')) {
      return sha1(file_get_contents($actualImagePath)) === sha1(file_get_contents($expectedImagePath));
    }

    // Create GD images from PNG files.
    $actualImage = imagecreatefrompng($actualImagePath);
    $expectedImage = imagecreatefrompng($expectedImagePath);

    // Get image dimensions.
    $width = imagesx($actualImage);
    $height = imagesy($actualImage);

    // Calculate Mean Absolute Error (MAE)
    $mae = 0;

    for ($y = 0; $y < $height; $y++) {
      for ($x = 0; $x < $width; $x++) {
        $pixelColor1 = imagecolorat($actualImage, $x, $y);
        $pixelColor2 = imagecolorat($expectedImage, $x, $y);

        // Extract RGB values.
        $r1 = ($pixelColor1 >> 16) & 0xFF;
        $g1 = ($pixelColor1 >> 8) & 0xFF;
        $b1 = $pixelColor1 & 0xFF;

        $r2 = ($pixelColor2 >> 16) & 0xFF;
        $g2 = ($pixelColor2 >> 8) & 0xFF;
        $b2 = $pixelColor2 & 0xFF;

        // Calculate absolute difference.
        $mae += abs($r1 - $r2) + abs($g1 - $g2) + abs($b1 - $b2);
      }
    }

    // Calculate Mean Absolute Error (MAE).
    $totalPixels = $width * $height;
    $mae /= (3 * $totalPixels);

    // Free up memory.
    imagedestroy($actualImage);
    imagedestroy($expectedImage);

    $threshold = 0.01;

    if ($mae > $threshold) {
      // Save the difference heatmap image.
      $diffImage = imagecreatetruecolor($width, $height);

      for ($y = 0; $y < $height; $y++) {
        for ($x = 0; $x < $width; $x++) {
          $actualImagePixelColor = imagecolorat($actualImage, $x, $y);
          $expectedImagePixelColor = imagecolorat($expectedImage, $x, $y);

          if ($actualImagePixelColor !== $expectedImagePixelColor) {
            // Mark pixel as broken with a red color.
            $brokenPixelColor = imagecolorallocatealpha($diffImage, 255, 0, 0, 20);
            imagesetpixel($diffImage, $x, $y, $brokenPixelColor);
          }
          else {
            // Set the modified color to the image.
            imagesetpixel($diffImage, $x, $y, $actualImagePixelColor);
          }
        }
      }

      // Save the diff image with the ".diff.png" suffix
      // in the same directory as $imagePath1.
      $diffImagePath = pathinfo($actualImagePath, PATHINFO_DIRNAME) . '/' . pathinfo($actualImagePath, PATHINFO_FILENAME) . '.diff.png';
      imagepng($diffImage, $diffImagePath);
      imagedestroy($diffImage);
    }

    return $mae <= $threshold;
  }

}
