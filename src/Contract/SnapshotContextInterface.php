<?php

declare(strict_types=1);

namespace Drupal\BehatSnapshots\Contract;

use Behat\Gherkin\Node\TableNode;

/**
 * Interface SnapshotContextContract.
 *
 * Interface SnapshotContextContract declares the public methods
 * that should be implemented and provide the possibility to test
 * website frontend pages.
 */
interface SnapshotContextInterface {

  /**
   * Function iTestListedPages.
   *
   * @param \Behat\Gherkin\Node\TableNode $pages
   *   List of pages.
   *
   * @return void
   *   Returns nothing.
   */
  public function iTestListedPages(TableNode $pages): void;

  /**
   * Function showPagesTestingReport.
   *
   * @return void
   *   Returns nothing.
   */
  public function showPagesTestingReport() : void;

}
