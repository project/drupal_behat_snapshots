<?php

declare(strict_types=1);

namespace Drupal\BehatSnapshots\Contract;

/**
 * Interface ResultRendererInterface.
 *
 * Interface ResultRendererInterface declares
 * the public methods for rendering results.
 */
interface ResultRendererInterface {

  /**
   * Constructor.
   */
  public function __construct(string $filePath);

  /**
   * Function render.
   *
   * This method is used to loop though the array of pages
   * and render readable result message.
   */
  public function render(array $pages);

}
