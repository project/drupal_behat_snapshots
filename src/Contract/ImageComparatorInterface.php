<?php

declare(strict_types=1);

namespace Drupal\BehatSnapshots\Contract;

/**
 * Interface ImageComparatorInterface.
 *
 * Interface ImageComparatorInterface declares
 * the methods available for image comparison.
 */
interface ImageComparatorInterface {

  /**
   * Function areImagesEqual.
   *
   * This method is used to compare two images based on their content.
   * Returns true if images are equal.
   *
   * @param string $actualImagePath
   *   Path to the first image.
   * @param string $expectedImagePath
   *   Path to the second image.
   *
   * @return bool
   *   Returns nothing.
   */
  public function areImagesEqual(string $actualImagePath, string $expectedImagePath): bool;

}
