# `Behat` installation.
# https://behat.org/en/latest/quick_start.html#installation
#
```bash
composer require --dev behat/behat
bin/behat -V
```

# `Behat Drupal Extension` installation.
# https://www.drupal.org/project/drupalextension
#
```bash
composer require --dev drupal/drupal-extension
```

# `Behat` dependencies installation.
#
#
```bash
composer require --dev behat/mink
composer require --dev behat/mink-browserkit-driver
```

# `Behat` init files.
#
#
```bash
bin/behat --init
```

# Create behat .feature files/structure.
#
#
```bash
mkdir -p tests/behat/features/drupal-behat-snapshots
```

# Execute tests.
#
#
```bash
bin/behat --config=behat.yml --colors --verbose --stop-on-failure
```
