# Usage Instructions:

## Step 1: Setting the list of pages to test.
Begin by updating the existing pages table with the specific pages you intend to test. You have the option to specify multiple tables and organize pages within them.

![Screenshot 1](https://git.drupalcode.org/project/drupal_behat_snapshots/-/raw/1.0.x/docs/images/screenshot-1.png)

## Step 2: Updating the folder structure.
If no folder(s) have been created for the anticipated page, the tool will raise an exception with the message "Please execute these commands to update the expected results."

To proceed and generate folders for the designated page(s), simply copy and paste the provided commands. The structure of the folders depends on the declared "Window breakpoints."

![Screenshot 2](https://git.drupalcode.org/project/drupal_behat_snapshots/-/raw/1.0.x/docs/images/screenshot-2.png)

## Step 3: Copying files into the expected folder for the first time.
Update the files in the folders. For new pages, copy and paste the suggested commands.

![Screenshot 3](https://git.drupalcode.org/project/drupal_behat_snapshots/-/raw/1.0.x/docs/images/screenshot-3.png)

## Step 4: Debug process. Checking .diff files in case of the test failure.
For existing pages, review the .diff files and determine whether you should copy and replace the expected files with the actual ones or address the identified issues.

![Screenshot 4](https://git.drupalcode.org/project/drupal_behat_snapshots/-/raw/1.0.x/docs/images/screenshot-4.png)

## Step 5: Passed test. Congrats!
If no errors are detected (meaning all expected files match the actual ones), you will successfully pass the checks.

![Screenshot 5](https://git.drupalcode.org/project/drupal_behat_snapshots/-/raw/1.0.x/docs/images/screenshot-5.png)
