# Contributing to the Drupal Behat Snapshots

Thank you for taking the time to contribute.

This page contains information about reporting issues as well as some tips and guidelines useful to experienced open source contributors.

## Reporting issues

A great way to contribute to the project is to send a detailed report when you encounter an issue. We always appreciate a well-written, thorough bug report, and will thank you for it.

Before submitting an issue please check that the [issue database](https://www.drupal.org/project/issues/drupal_behat_snapshots) does not already contain the same problem or suggestion.

If you find a match you can use the "subscribe" button to get notified on updates. Please do not leave random "+1" or "I have this too" comments because they clutter the discussion and don't help resolve it. However, if you have ways to reproduce the issue or have additional information that may help resolve the issue then please leave a comment.

## Contributing code

Pull requests are always welcome. Not sure if that typo is worth a pull request? Found a bug and know how to fix it? Do it! We appreciate the help. Any significant improvement should be documented as [an issue](https://www.drupal.org/project/issues/drupal_behat_snapshots) before starting working on it.

We are always thrilled to receive pull requests, and we do our best to process them quickly and provide feedback.
