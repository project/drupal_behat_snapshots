# Drupal Behat Snapshots

## Description

This package is a Behat extension that implements an additional "Drupal\BehatSnapshots\Context\SnapshotContext" Context to make it possible to test frontend with a snapshot testing approach.
We suggest considering using this testing approach when it's needed to control some static page's look.

### Idea
The idea of this approach is quite simple. Here is an algorithm of the snapshot testing approach in a few steps:

- Ensure that the application page is manually tested an application and is ready to be screenshotted(snapshot).
- Run the script(test) to generate the expected page screenshots automatically
  - The tool should be able to scroll the page in both directions if needed.
- Save the actual and expected results in the repository(project filesystem).
  - The tool provides the console commands to save it.
- Change the system and re-run the test.
- Compare actual(new) page screenshots with expected ones.
  - Service that implements image comparison logic implements Drupal\BehatSnapshots\Contract\ImageComparatorInterface contract, which declares that the areImagesEqual method should accept 2 image paths - excepted & actual and return TRUE|FALSE as a comparison result.
- If we detect, that some images are different, we show an error and propose to replace the expected snapshot with an actual one.

Automated testing provides the possibility to generate and update page screenshot folders automatically, with no need to create screenshots manually.
All you need is to declare the list of pages and settings for the testing in the `.feature` file.

## Testing .feature scenario example

Please take a look at the ./features/snapshot.feature example .feature file.
The table that contains a list of pages also contains optional settings that could be applied. Below you can find the description of the table columns.

### Page name
Required.
Example: User login page
Description: This is the name of the page. It's used to generate the folder name for storing the actual & expected results.

### Page path
Required.
Example: /user/login
Description: UTR path of the page. Please do not specify the absolute URL, the relative path is required.

### Wait for page to load
Optional.
Example: 2
Description: Force test to wait some period in seconds before making a snapshot of the page.

### Wait for text
Optional.
Example: Congratulations and welcome to the Drupal community.
Description: Force test to wait some period in seconds before some text is visible on the page.

### Wait for an element to be visible
Optional.
Example: input[type=name].
Description: Force test to wait some in seconds before some element is visible. Specify the list of CSS selectors separated by commas.


### Hide elements
Optional.
Example: #block-olivero-syndicate.
Description: The most known bottleneck of this kind of testing is the dynamic data on the page. If we can not rely on the testing data(i.e. it could be dynamically generated during the build process), snapshot testing is probably not a suitable solution.
Use CSS selector to hide some elements.

### Hide all images
Optional.
Example: false.
Description: This setting is similar to the `Hide elements` setting, but allows to hide all the images on the page at once.
Because the images will be different each time we create a new website build.
However, if there are some dynamic images on the page, we can use the smart technique to hide them before making a screenshot. In this case, the images will be the same each time we run the test.

### Window breakpoints
Optional.
Example: 300, 600, 800, 1200
Description: Breakpoints are customizable widths that determine how your responsive layout behaves across device or viewport sizes. It's possible to run checks on 1 or more breakpoints using the coma separator.
Feel free to add as much as you need, but please keep an eye on the test execution performance also.

## Behat tests. SnapshotContext usage instructions

### 1. Assuming you already have Behat installed via Composer.

Take a look into the Behat initial installation instructions on the Drupal project if required [here](https://git.drupalcode.org/project/drupal_behat_snapshots/-/blob/1.0.x/docs/BEHAT_INSTALLATION.md).

### 2. Install the Behat contexts using composer. Apply changes to the composer.json and run `composer update drupal/drupal-behat-snapshots`.

```
    "repositories": {
        ...
        "drupal/drupal-behat-snapshots": {
            "type": "git",
            "url": "https://git.drupalcode.org/project/drupal_behat_snapshots.git"
        }
    },

   "require-dev": {
      ...
     "drupal/drupal-behat-snapshots": "^1.0.0"
   }
```

### 3. Add the new contexts to your Behat configuration, e.g.:

```yml
default:
  suites:
    default:
      contexts:
        # do not forget to comment out MinkContext, because we extend it do not want to have duplications.
        #-Drupal\DrupalExtension\Context\MinkContext
        - Drupal\BehatSnapshots\Context\SnapshotContext
```

### 4. Copy the default feature file (representing the out-of-the-box configuration) to your `features` directory. See the example below.

```bash

mkdir -p tests/behat/features/drupal-behat-snapshots
cp vendor/drupal/drupal-behat-snapshots/features/*.feature tests/behat/features/drupal-behat-snapshots/
cp vendor/drupal/drupal-behat-snapshots/behat.yml.dist behat.yml
```

### 5. Change the specification according to the requirements, and update the features to match using the generated Gherkin.

### 6. Run Behat! If the tests pass, your application matches the specifications. If not, change one or the other according to your needs.

### 7. Execute the test.
```bash
bin/behat --config=behat.yml --colors --verbose --stop-on-failure ./tests/behat/features/drupal-behat-snapshots/snapshot.feature
```

### 8. Follow the [usage instructions](https://git.drupalcode.org/project/drupal_behat_snapshots/-/blob/1.0.x/docs/USAGE_INSTRUCTIONS.md).
